# classifier



## Getting started

Classifier is a python application with GUI. Project is using machine learning to classify objects.



## Installation
1. Clone the repository or download the script:

    ```bash
    git clone https://gitlab.com/K.Skylark/classifier
    ```


2. Navigate to the project directory:

    ```bash
    cd classifier
    ```

3. Install the required Python packages:

    If using `pip`:

    ```bash
    pip install -r requirements.txt
    ```

    If using `conda`:

    ```bash
    conda create --name myenv --file requirements.txt
    conda activate myenv

## Usage
```
python classifier.py
```